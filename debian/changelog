erlang-lager (3.8.1-3) unstable; urgency=medium

  * Corrected Multi-Arch setting to "allowed"

 -- Philipp Huebner <debalance@debian.org>  Sun, 31 Jan 2021 17:51:24 +0100

erlang-lager (3.8.1-2) unstable; urgency=medium

  * Added 'Multi-Arch: same' in debian/control
  * Updated years in debian/copyright

 -- Philipp Huebner <debalance@debian.org>  Sat, 30 Jan 2021 16:08:43 +0100

erlang-lager (3.8.1-1) unstable; urgency=medium

  * New upstream version 3.8.1
  * Updated Standards-Version: 4.5.1 (no changes needed)
  * Updated Erlang dependencies
  * Updated version of debian/watch: 4
  * Updated debhelper compat version: 13
  * Updated lintian overrides
  * Added debian/erlang-lager.install
  * Refreshed debian/patches/remove-deps.diff

 -- Philipp Huebner <debalance@debian.org>  Fri, 25 Dec 2020 21:39:08 +0100

erlang-lager (3.8.0-2) unstable; urgency=medium

  * Updated debhelper compat version: 12
  * Updated Standards-Version: 4.5.0 (no changes needed)
  * Updated years in debian/copyright
  * Rules-Requires-Root: no

 -- Philipp Huebner <debalance@debian.org>  Tue, 04 Feb 2020 21:08:30 +0100

erlang-lager (3.8.0-1) unstable; urgency=medium

  * New upstream version 3.8.0

 -- Philipp Huebner <debalance@debian.org>  Sat, 10 Aug 2019 13:42:19 +0200

erlang-lager (3.7.0-1) unstable; urgency=medium

  * New upstream version 3.7.0
  * Updated Standards-Version: 4.4.0 (no changes needed)
  * Updated Erlang dependencies
  * Updated debhelper compat version: 12
  * debian/rules: export ERL_COMPILER_OPTIONS=deterministic

 -- Philipp Huebner <debalance@debian.org>  Wed, 24 Jul 2019 11:42:07 +0200

erlang-lager (3.6.8-1) unstable; urgency=medium

  * New upstream version 3.6.8
  * Updated Standards-Version: 4.3.0 (no changes needed)
  * Updated years in debian/copyright

 -- Philipp Huebner <debalance@debian.org>  Tue, 01 Jan 2019 22:28:24 +0100

erlang-lager (3.6.6-1) unstable; urgency=medium

  * New upstream version 3.6.6
  * Updated Standards-Version: 4.2.1 (no changes needed)

 -- Philipp Huebner <debalance@debian.org>  Sun, 07 Oct 2018 14:31:24 +0200

erlang-lager (3.6.4-2) unstable; urgency=medium

  * Don't fail building if eunit fails, the test framework is not stable

 -- Philipp Huebner <debalance@debian.org>  Sun, 15 Jul 2018 15:40:03 +0200

erlang-lager (3.6.4-1) unstable; urgency=medium

  * New upstream version 3.6.4
  * Updated Standards-Version: 4.1.5 (no changes needed)

 -- Philipp Huebner <debalance@debian.org>  Sun, 15 Jul 2018 14:35:36 +0200

erlang-lager (3.6.3-1) unstable; urgency=medium

  * New upstream version 3.6.3

 -- Philipp Huebner <debalance@debian.org>  Wed, 04 Jul 2018 19:40:45 +0200

erlang-lager (3.6.2-1) unstable; urgency=medium

  * New upstream version 3.6.2
  * Updated Standards-Version: 4.1.4 (no changes needed)

 -- Philipp Huebner <debalance@debian.org>  Wed, 09 May 2018 12:45:37 +0200

erlang-lager (3.6.1-2) unstable; urgency=medium

  * Upload to unstable
  * Use secure copyright format uri in debian/copyright
  * Switched to debhelper 11

 -- Philipp Huebner <debalance@debian.org>  Wed, 28 Mar 2018 00:14:25 +0200

erlang-lager (3.6.1-1) experimental; urgency=medium

  * New upstream version 3.6.1

 -- Philipp Huebner <debalance@debian.org>  Tue, 06 Feb 2018 21:02:30 +0100

erlang-lager (3.6.0-1) experimental; urgency=medium

  * New upstream version 3.6.0

 -- Philipp Huebner <debalance@debian.org>  Sat, 27 Jan 2018 14:14:47 +0100

erlang-lager (3.5.2-2) unstable; urgency=medium

  * Set Maintainer: Ejabberd Packaging Team <ejabberd@packages.debian.org>
  * Set Uploaders: Philipp Huebner <debalance@debian.org>
  * Updated Vcs-* fields in debian/control for salsa.debian.org
  * Updated years in debian/copyright
  * Updated Standards-Version: 4.1.3 (no changes needed)

 -- Philipp Huebner <debalance@debian.org>  Thu, 04 Jan 2018 11:37:43 +0100

erlang-lager (3.5.2-1) unstable; urgency=low

  * New upstream version 3.5.2
  * Updated Standards-Version: 4.1.1 (no changes needed)

 -- Philipp Huebner <debalance@debian.org>  Sun, 22 Oct 2017 09:44:51 +0200

erlang-lager (3.5.1-1) unstable; urgency=medium

  * New upstream version 3.5.1
  * Updated years in debian/copyright
  * Updated Standards-Version: 4.0.0 (no changes needed)
  * (Build-)Depend on erlang-goldrush (>= 0.1.9-3~)
  * Switched source to https://github.com/erlang-lager/lager

 -- Philipp Huebner <debalance@debian.org>  Tue, 11 Jul 2017 10:24:44 +0200

erlang-lager (3.2.4-1) unstable; urgency=medium

  * New upstream version 3.2.4 (Closes: #841262)
  * Added erlang-base to Build-Depends
  * (Build-)Depend on erlang-goldrush (>= 0.1.9)
  * Enabled eunit tests

 -- Philipp Huebner <debalance@debian.org>  Wed, 19 Oct 2016 07:39:38 +0200

erlang-lager (3.2.1-2) unstable; urgency=medium

  * Build-Depend on erlang-crypto (fixes FTBFS)

 -- Philipp Huebner <debalance@debian.org>  Wed, 07 Sep 2016 11:40:05 +0200

erlang-lager (3.2.1-1) unstable; urgency=medium

  * Imported Upstream version 3.2.1

 -- Philipp Huebner <debalance@debian.org>  Sun, 26 Jun 2016 15:38:34 +0200

erlang-lager (3.2.0-1) unstable; urgency=medium

  * Imported Upstream version 3.2.0
  * Improved debian/watch
  * Updated Standards-Version: 3.9.8 (no changes needed)
  * Updated Vcs-* fields in debian/control

 -- Philipp Huebner <debalance@debian.org>  Sun, 29 May 2016 20:37:00 +0200

erlang-lager (3.1.0-1) unstable; urgency=medium

  * Updated debian/copyright
  * Imported Upstream version 3.1.0

 -- Philipp Huebner <debalance@debian.org>  Sun, 31 Jan 2016 13:38:21 +0100

erlang-lager (3.0.2-1) unstable; urgency=low

  * Imported Upstream version 3.0.2

 -- Philipp Huebner <debalance@debian.org>  Sat, 05 Dec 2015 14:47:14 +0100

erlang-lager (3.0.1-1) unstable; urgency=low

  * Streamlined debian/rules
  * Imported Upstream version 3.0.1

 -- Philipp Huebner <debalance@debian.org>  Mon, 17 Aug 2015 20:09:52 +0200

erlang-lager (2.1.1-2) unstable; urgency=low

  * Avoid dependency on erlang-common-test (Closes: #783809)

 -- Philipp Huebner <debalance@debian.org>  Fri, 01 May 2015 10:21:59 +0200

erlang-lager (2.1.1-1) unstable; urgency=low

  * New upstream release
  * Updated debian/watch
  * Updated Standards-Version: 3.9.6 (no changes needed)
  * Updated years in debian/copyright
  * Added Vcs links to debian/control

 -- Philipp Huebner <debalance@debian.org>  Mon, 27 Apr 2015 09:51:50 +0200

erlang-lager (2.0.3-1) unstable; urgency=low

  * Initial release (Closes: #744880)

 -- Philipp Huebner <debalance@debian.org>  Tue, 22 Apr 2014 20:43:25 +0200
